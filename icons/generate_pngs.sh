#!/bin/bash

out_dir=$1

function generate_png {
	convert -density `expr $2 / 48 \* 96` -resize ${2}x${2} -background none $1 ${out_dir}${1%.*}_${2}.png &> /dev/null
}

for svg in *.svg; do
	for size in 16 24; do
		generate_png $svg $size
	done
done

for size in 16 24 32 48 64 128 256 512 1024; do
	generate_png "app_icon.jpg" $size
done
