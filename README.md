# If I Fits, I Sits

![](src/resources/icons/app_icon_256.png)

"If I Fits, I Sits" is a seating planner program created for an HSC Software Design and Development project. It is written in Java 8 with JavaFX.

Features include:

- 6 Built-in seating layouts
- Loading and saving seating plans
- Printing seating plans
- Adding students one at a time or multiple at a time
- Automatically seating students alphabetically or randomly
- Swapping two students

## Releases

- [v1.0](https://gitlab.com/sommd/if-i-fits-i-sits/tags/v1.0)

## Building

1. Clone repo
2. Open project in IntelliJ Idea
3. Build > Build Artifacts... > jar > Build
4. Built jar is in `out/artifacts/jar/If I Fits, I Sits.jar`

## Help

### User Interface

#### Overview

![](src/resources/info/help_overview.png)

#### Students Table

The students in your class will be shown here. Clicking the column titles ("Given Name", "Surname" and "Seat") will sort the table by that column.

#### Class Layout Preview

A preview of the class will be shown here. Each blue square represents a seat. Students' names will be shown on the seat they are assigned.

#### Toolbar

The toolbar has some buttons to quickly perform actions. From left to right the buttons are:

- New Seating Plan
- Open Seating Plan
- Save Seating Plan
- Add New Student
- Add Multiple New Students
- Delete Selected Students
- Swap Selected Students
- Help

##### Seating Layout Selector

![](src/resources/info/seating_layout_selector.png)

This menu allows you to select different seating layouts.

#### Menu Bar

The menu bar contains actions for the program.

- File
    - New Seating Plan
        - Start a new seating plan, disposing of the current one.
    - Open Seating Plan
        - Open a saved seating plan, disposing of the current one. You can open both "If I Fits, I Sits Seating Plan" files (.plan format) and "Classic Seating Plan" files (.txt format).
    - Save Seating Plan
        - Saves the current seating plan.
    - Save Seating Plan As
        - Saves the current seating plan to a new file. You can save both as a "If I Fits, I Sits Seating Plan" file (.plan format) or "Classic Seating Plan" file (.txt format).
    - Print Seating Plan
        - Prints the current seating plan.
    - Exit
        - Exit the program.
- Edit
    - New Student
        - Add a new student to the class.
    - New Students
        - Add multiple students to the class at once.
    - Delete Selected Students
        - Delete the currently selected students in the students table.
    - Swap Selected Students Seats'
        - When 2 students are selected, swap their seats.
    - Assign Students Alphabetically
        - Assign all students to seats in alphabetical order.
    - Assign Students Random
        - Assign all students to seats randomly.
- Help
    - Help
        - Show this help screen.
    - About
        - Show the about screen.

### Editing Students

To add a student you can either **select "New Student" from the "Edit" menu or the on the toolbar**. This will add a new student to the students table and you can then add a name and assign them a seat.

You can also **select "New Students" from the "Edit" menu** to add multiple students at once.

![](src/resources/info/add_students.png)

In the students table you can **select students by clicking them**, and **select multiple students by holding control and clicking them**. **Double click a cell to edit its value**, e.g. double click the first name of a student to edit their name.

To remove a student from their assigned seat, double click the seat in the students table and delete the number so it's empty.

**Right click** to open the menu: ![](src/resources/info/students_table_menu.png)
