// set cell factories so that they are editable

seatColumn.setCellFactory(controller.getSeatingPlan().createStudentCellFactory());

// enable multiple selection
studentsTable.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);

// create binding for number of selected students
var selectedAmountBinding = javafx.beans.binding.Bindings.size(studentsTable.getSelectionModel().getSelectedCells());

// bind delete menu items so they are disable when nothing is selected
[deleteStudentsMenuItem, deleteStudentsContextMenuItem, deleteStudentsButton].forEach(function(item){
	item.disableProperty().bind(selectedAmountBinding.isEqualTo(0));
});

// bind swap menu item so it is disabled when there aren't two students selected
[swapStudentsMenuItem, swapStudentsContextMenuItem, swapStudentsButton].forEach(function(item){
	item.disableProperty().bind(selectedAmountBinding.isNotEqualTo(2));
});
