package com.djs.utils.javafx;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public final class MessageBox{
	private MessageBox(){}
	
	public static Alert showMessage(Alert.AlertType type, String title, String text){
		Alert alert = new Alert(type, text);
		alert.setTitle(title);
		alert.setHeaderText(title);
		alert.show();
		
		return alert;
	}
	
	public static boolean showConfirm(String title, String text){
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION, text, ButtonType.YES, ButtonType.CANCEL);
		alert.setTitle(title);
		alert.setHeaderText(title);
		
		return alert.showAndWait()
				.filter(ButtonType.YES::equals)
				.isPresent();
	}
	
	public static Alert createCustom(String title, Node content){
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setHeaderText(title);
		alert.getDialogPane().setContent(content);
		
		return alert;
	}
}
