package com.djs.seatify.backend;

import com.djs.utils.javafx.MessageBox;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class SeatingPlan{
	public class Seat implements Comparable{
		private class StudentProperty extends SimpleObjectProperty<Student>{
			/** Sets this property without updating the student's seat to this, used by {@link Student.SeatProperty#set(Seat)} */
			public void setDirect(Student newValue){
				super.set(newValue);
			}
			
			@Override
			public void set(Student newValue){
				Student old = get();
				
				// only update if the student will change
				if(newValue != old){
					super.set(newValue); // set the student
					
					if(newValue != null){
						newValue.seat.set(Seat.this); // set the new student's seat to be this student
					}
					
					if(old != null){
						// set the old student's desk to be null, without having it it's desk's (this) student to be null
						old.seat.setDirect(null);
					}
				}
			}
		}
		
		private final IntegerProperty positionX, positionY;
		private final ReadOnlyIntegerProperty number;
		private final StudentProperty student;
		
		public Seat(){
			positionX = new SimpleIntegerProperty();
			positionY = new SimpleIntegerProperty();
			
			number = new SimpleIntegerProperty();
			// bind this seat's number to be the seat's index in the seats array
			((IntegerProperty) number).bind(Bindings.createIntegerBinding(() -> seats.indexOf(this), seats));
			
			student = new StudentProperty();
		}
		
		public int getPositionX(){
			return positionX.get();
		}
		
		public IntegerProperty positionXProperty(){
			return positionX;
		}
		
		public void setPositionX(int positionX){
			this.positionX.set(positionX);
		}
		
		public int getPositionY(){
			return positionY.get();
		}
		
		public IntegerProperty positionYProperty(){
			return positionY;
		}
		
		public void setPositionY(int positionY){
			this.positionY.set(positionY);
		}
		
		public int getNumber(){
			return number.get();
		}
		
		public ReadOnlyIntegerProperty numberProperty(){
			return number;
		}
		
		public Student getStudent(){
			return student.get();
		}
		
		public ObjectProperty<Student> studentProperty(){
			return student;
		}
		
		public void setStudent(Student student){
			this.student.set(student);
		}
		
		@Override
		public int compareTo(Object o){
			return Integer.compare(getNumber(), o instanceof Seat ? ((Seat) o).getNumber() : 0);
		}
	}
	
	public class Student{
		private class SeatProperty extends SimpleObjectProperty<Seat>{
			/** Sets this property without updating the seat's student to this, used by {@link Seat.StudentProperty#set(Student)} */
			public void setDirect(Seat newValue){
				super.set(newValue);
			}
			
			@Override
			public void set(Seat newValue){
				Seat old = get();
				
				// only update if the seat will change
				if(newValue != old){
					super.set(newValue); // set the student
					
					if(newValue != null){
						newValue.student.set(Student.this); // set the new seat's student to be this student
					}
					
					if(old != null){
						// set the old seat's student to be null, with it settings it's seat (this) to be null
						old.student.setDirect(null);
					}
				}
			}
		}
		
		private final StringProperty firstName, lastName;
		private final SeatProperty seat;
		
		public Student(){
			firstName = new SimpleStringProperty("No Name");
			lastName = new SimpleStringProperty("No Name");
			
			seat = new SeatProperty();
		}
		
		public String getFirstName(){
			return firstName.get();
		}
		
		public StringProperty firstNameProperty(){
			return firstName;
		}
		
		public void setFirstName(String firstName){
			this.firstName.set(firstName);
		}
		
		public String getLastName(){
			return lastName.get();
		}
		
		public StringProperty lastNameProperty(){
			return lastName;
		}
		
		public void setLastName(String lastName){
			this.lastName.set(lastName);
		}
		
		public Seat getSeat(){
			return seat.get();
		}
		
		public ObjectProperty<Seat> seatProperty(){
			return seat;
		}
		
		public void setSeat(Seat seat){
			this.seat.set(seat);
		}
	}
	
	private final IntegerProperty width, height;
	
	private final ObservableList<Seat> seats;
	private final ObservableList<Student> students;
	
	public SeatingPlan(){
		width = new SimpleIntegerProperty();
		height = new SimpleIntegerProperty();
		
		seats = FXCollections.observableArrayList();
		students = FXCollections.observableArrayList();
	}
	
	public int getWidth(){
		return width.get();
	}
	
	public IntegerProperty widthProperty(){
		return width;
	}
	
	public void setWidth(int width){
		this.width.set(width);
	}
	
	public int getHeight(){
		return height.get();
	}
	
	public IntegerProperty heightProperty(){
		return height;
	}
	
	public void setHeight(int height){
		this.height.set(height);
	}
	
	public ObservableList<Seat> getSeats(){
		return seats;
	}
	
	public ObservableList<Student> getStudents(){
		return students;
	}
	
	/**
	 * Create a new student and add it to this seating plan.
	 *
	 * @return The new student
	 */
	public Student newStudent(){
		Student student = new Student();
		students.add(student);
		return student;
	}
	
	/**
	 * Create a new seat and add it to this seating plan.
	 *
	 * @return The new seat
	 */
	public Seat newSeat(){
		Seat seat = new Seat();
		seats.add(seat);
		return seat;
	}
	
	/** Remove all the students from this seating plan */
	public void clearStudents(){
		students.clear();
		// clear the seats' students
		seats.forEach(s -> s.setStudent(null));
	}
	
	/** Remove all the seats from this seating plan */
	public void clearSeats(){
		seats.clear();
		// clear the students' seats
		students.forEach(s -> s.setSeat(null));
	}
	
	/** Remove all the seats and the students from this seating plan */
	public void clearAll(){
		students.clear();
		seats.clear();
	}
	
	/** Writes the seating plan to an output stream */
	public void write(OutputStream out) throws IOException{
		DataOutputStream dos = new DataOutputStream(out);
		
		// write the number of seats
		dos.writeInt(seats.size());
		for(Seat seat : seats){
			// write each seat's position
			dos.writeInt(seat.getPositionX());
			dos.writeInt(seat.getPositionY());
		}
		
		// write the number of students
		dos.writeInt(students.size());
		for(Student student : students){
			// write each student's name
			dos.writeUTF(student.getFirstName());
			dos.writeUTF(student.getLastName());
			// write each student's desk number, or -1 if no desk
			dos.writeInt(student.getSeat() != null ? student.getSeat().getNumber() : -1);
		}
	}
	
	/** Write the seating plan to an output stream, using the old .txt format */
	public void writeLegacy(OutputStream out) throws IOException{
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
		
		// write each student based in order of desk
		for(Seat seat : seats){
			Student student = seat.getStudent();
			
			if(student != null){ // if this desk has a student, write their name
				writer.write(student.getLastName());
				writer.write(", ");
				writer.write(student.getFirstName());
			}
			
			// next line, even if there was no student (so this desk is empty)
			writer.newLine();
		}
		
		// flush the writer to the output stream since the writer is buffered, otherwise the output stream is closed while the writer is
		// still holding the data in it's buffer
		writer.flush();
	}
	
	/** Reads a seating plan from an input stream written by {@link #write(OutputStream)} */
	public void read(InputStream in) throws IOException{
		// clear the seating plan
		clearAll();
		
		DataInputStream dis = new DataInputStream(in);
		
		// read the number of seats
		int numSeats = dis.readInt();
		for(int i = 0; i < numSeats; i++){
			// create each seat in its position
			Seat seat = newSeat();
			seat.setPositionX(dis.readInt());
			seat.setPositionY(dis.readInt());
		}
		
		// read the number of students
		int numStudents = dis.readInt();
		for(int i = 0; i < numStudents; i++){
			// create each student, setting their name
			Student student = newStudent();
			student.setFirstName(dis.readUTF());
			student.setLastName(dis.readUTF());
			
			// read the seat number
			int seatNumber = dis.readInt();
			if(seatNumber != -1){ // if they had a seat, set their seat
				student.setSeat(getSeats().get(seatNumber));
			}
		}
	}
	
	/** Reads a seating plan from an input stream written by {@link #writeLegacy(OutputStream)}. The current seats will be kept. */
	public void readLegacy(InputStream in) throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		
		clearStudents();
		
		// read the file line by line
		String line;
		for(int i = 0; (line = reader.readLine()) != null; i++){
			String[] names = line.split(", ", 2); // split the line into first name and last name 
			
			if(names.length == 2){ // check the name was in the correct format
				// create the student and set their name
				Student student = newStudent();
				student.setLastName(names[0]);
				student.setFirstName(names[1]);
				
				// assign the student a seat based on the line number in the file if there are enough seats
				student.setSeat(getSeats().size() > i ? getSeats().get(i) : null);
			}
		}
	}
	
	/** Creates a table cell factory which creates the cells of a table for each item. */
	public Callback<TableColumn<Student, Seat>, javafx.scene.control.TableCell<Student, Seat>> createStudentCellFactory(){
		return TextFieldTableCell.forTableColumn(new StringConverter<Seat>(){
			@Override
			public String toString(Seat object){
				// return the seat number or "No Seat" if they do not have a seat
				return object != null ? Integer.toString(object.getNumber() + 1) : "No Seat";
			}
			
			@Override
			public Seat fromString(String string){
				try{
					// try to parse the seat number, and return that seat, or if the input was empty, return null
					return string.isEmpty() ? null : seats.get(Integer.parseInt(string) - 1);
				}catch(NumberFormatException e){ // if the input wasn't a number
					MessageBox.showMessage(Alert.AlertType.ERROR, "Invalid Number",
										   "Enter a number to select a seat, or enter nothing clear the current seat.");
					
					// propagate error to kill thread so that text field remains focused
					throw e;
				}catch(IndexOutOfBoundsException e){// if the seat doesn't exist
					MessageBox.showMessage(Alert.AlertType.ERROR, "Invalid Seat",
										   "There is no seat number " + string.trim() + ".");
					
					// propagate error to kill thread so that text field remains focused
					throw e;
				}
			}
		});
	}
}
