package com.djs.seatify.backend;

import javafx.beans.DefaultProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

@DefaultProperty("seats")
public class SeatingLayout{
	public static class Seat{
		private final IntegerProperty x, y;
		
		public Seat(){
			x = new SimpleIntegerProperty();
			y = new SimpleIntegerProperty();
		}
		
		public int getX(){
			return x.get();
		}
		
		public IntegerProperty xProperty(){
			return x;
		}
		
		public void setX(int x){
			this.x.set(x);
		}
		
		public int getY(){
			return y.get();
		}
		
		public IntegerProperty yProperty(){
			return y;
		}
		
		public void setY(int y){
			this.y.set(y);
		}
	}
	
	private static final int DEFAULT_WIDTH = 1;
	private static final int DEFAULT_HEIGHT = 1;
	
	private final ObservableList<Seat> seats;
	private final IntegerProperty width, height;
	private final StringProperty name;
	
	public SeatingLayout(){
		seats = FXCollections.observableArrayList();
		
		width = new SimpleIntegerProperty(DEFAULT_WIDTH);
		height = new SimpleIntegerProperty(DEFAULT_HEIGHT);
		
		name = new SimpleStringProperty();
	}
	
	public ObservableList<Seat> getSeats(){
		return seats;
	}
	
	public int getWidth(){
		return width.get();
	}
	
	public IntegerProperty widthProperty(){
		return width;
	}
	
	public void setWidth(int width){
		this.width.set(width);
	}
	
	public int getHeight(){
		return height.get();
	}
	
	public IntegerProperty heightProperty(){
		return height;
	}
	
	public void setHeight(int height){
		this.height.set(height);
	}
	
	public String getName(){
		return name.get();
	}
	
	public StringProperty nameProperty(){
		return name;
	}
	
	public void setName(String name){
		this.name.set(name);
	}
	
	/** Applies this {@link SeatingLayout} to a {@link SeatingPlan}, updating the seats to match */
	public void apply(SeatingPlan plan){
		plan.clearSeats();
		
		plan.setWidth(getWidth());
		plan.setHeight(getHeight());
		
		for(Seat position: seats){
			SeatingPlan.Seat seat = plan.newSeat();
			seat.setPositionX(position.getX());
			seat.setPositionY(position.getY());
		}
	}
	
	@Override
	public String toString(){
		return String.format("%s (%d Seats)", getName(), getSeats().size());
	}
}
