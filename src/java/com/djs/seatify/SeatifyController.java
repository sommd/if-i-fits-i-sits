package com.djs.seatify;

import com.djs.seatify.backend.SeatingLayout;
import com.djs.seatify.backend.SeatingPlan;
import com.djs.utils.javafx.MessageBox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;

public class SeatifyController{
	/** List of file extensions for saving/loading */
	private static final FileChooser.ExtensionFilter[] FILE_EXTENSIONS = {
			new FileChooser.ExtensionFilter("All Seating Plans", "*.plan", "*.txt"),
			new FileChooser.ExtensionFilter("If I Fits, I Sits Seating Plan", "*.plan"),
			new FileChooser.ExtensionFilter("Classic Seating Plan", "*.txt"),
	};
	
	private static final String DEFAULT_FILE_NAME = "Seating Plan.plan";
	
	private @FXML ComboBox<SeatingLayout> seatingLayoutComboBox;
	private @FXML TableView<SeatingPlan.Student> studentsTable;
	private @FXML SeatingPlanView seatingPlanView;
	
	/** The current seating plan. */
	private final SeatingPlan seatingPlan;
	/** The currently 'open' file. Set when saving or when a file is opened. */
	private File currentFile;
	
	public SeatifyController(){
		seatingPlan = new SeatingPlan();
	}
	
	public SeatingPlan getSeatingPlan(){
		return seatingPlan;
	}
	
	public void initialize(){
		studentsTable.setItems(seatingPlan.getStudents());
		
		seatingPlanView.setSeatingPlan(seatingPlan);
		
		// Listen for changes to the selected seating plan
		ChangeListener<SeatingLayout> changeListener = (o, old, layout) -> layout.apply(seatingPlan);
		seatingLayoutComboBox.valueProperty().addListener(changeListener);
		
		// call the listener, so that the current layout is set
		changeListener.changed(null, null, seatingLayoutComboBox.getValue());
	}
	
	public void newSeatingPlan(){
		// confirm before creating new plan
		if(MessageBox.showConfirm("New seating plan", "Are you sure you want to create a new seating plan? Any unsaved changed to your " +
													  "current seating plan will be lost.")){
			// clear seating plan
			seatingPlan.clearStudents();
			
			// 'close' the current file
			currentFile = null;
		}
	}
	
	public void openSeatingPlan(){
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().addAll(FILE_EXTENSIONS);
		chooser.setInitialFileName(DEFAULT_FILE_NAME);
		
		File file = chooser.showOpenDialog(seatingPlanView.getScene().getWindow());
		
		if(file != null){
			try(FileInputStream fis = new FileInputStream(file)){
				if(file.getName().endsWith(".txt")){
					// save legacy format if .txt
					seatingPlan.readLegacy(fis);
				}else{
					// decent format otherwise
					seatingPlan.read(fis);
				}
				
				currentFile = file;
			}catch(FileNotFoundException e){
				MessageBox.showMessage(Alert.AlertType.ERROR, "Error Opening File", "Could not open selected file.");
			}catch(IOException e){
				MessageBox.showMessage(Alert.AlertType.ERROR, "Error Loading File",
									   "There was an error loading the file. The file may be corrupt");
			}
		}
	}
	
	public void saveSeatingPlan(){
		if(currentFile != null){ // check if a file is 'open'
			try(FileOutputStream fos = new FileOutputStream(currentFile)){
				if(currentFile.getName().endsWith(".txt")){
					seatingPlan.writeLegacy(fos);
				}else{
					seatingPlan.write(fos);
				}
			}catch(FileNotFoundException e){
				MessageBox.showMessage(Alert.AlertType.ERROR, "Error Saving File", "Could not save seating plan to this file.");
			}catch(IOException e){
				MessageBox.showMessage(Alert.AlertType.ERROR, "Error Saving File",
									   "An unknown error occurred when saving the file. Sorry.");
			}
		}else{ // show file selection otherwise
			saveSeatingPlanAs();
		}
	}
	
	public void saveSeatingPlanAs(){
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().addAll(FILE_EXTENSIONS);
		chooser.setInitialFileName(DEFAULT_FILE_NAME);
		
		File file = chooser.showSaveDialog(seatingPlanView.getScene().getWindow());
		
		if(file != null){ // make sure a file was selected
			if(file.getName().endsWith(".txt")){
				// warn about saving as legacy format
				if(!MessageBox.showConfirm("Legacy file format",
										   "Are you sure you want to save the file as this format? Only students names and seat " +
										   "numbers will be save, seating layout information will be lost.")){
					// go back to file chooser if they cancel
					saveSeatingPlanAs();
					return;
				}
			}
			
			currentFile = file;
			
			// save to the new current file
			saveSeatingPlan();
		}
	}
	
	public void printSeatingPlan(){
		PrinterJob job = PrinterJob.createPrinterJob();
		
		if(job != null){
			Printer printer = job.getPrinter();
			PageLayout defaultLayout = printer.createPageLayout(Paper.A4,
					PageOrientation.LANDSCAPE,
					Printer.MarginType.DEFAULT);
			job.getJobSettings().setPageLayout(defaultLayout);
			
			if(job.showPrintDialog(seatingPlanView.getScene().getWindow())){
				SeatingPlanView view = new SeatingPlanView();
				view.setSeatingPlan(seatingPlan);
					
				PageLayout layout = job.getJobSettings().getPageLayout();
				view.setPrefSize(layout.getPrintableWidth(), layout.getPrintableHeight());
				
				if(job.printPage(view)){
					job.endJob();
				}else{
					MessageBox.showMessage(Alert.AlertType.ERROR, "Printing Error",
							"An unknown printing error occurred.");
				}
			}
		}else{
			MessageBox.showMessage(Alert.AlertType.ERROR, "Printing Error", "No printer was found");
		}
	}
	
	public void exit(){
		// confirm before exiting
		if(MessageBox.showConfirm("Exit", "Are you sure you want to exit? Any unsaved changes will be lost.")){
			Platform.exit();
		}
	}
	
	public void newStudent(){
		seatingPlan.newStudent();
	}
	
	public void newStudents(){
		// create entry form for entering multiple students
		
		VBox vbox = new VBox(8);
		
		vbox.getChildren().add(new Label("Enter students names in format \"<Last Name>, <First Name>\", one name per line."));
		
		TextArea textArea = new TextArea();
		vbox.getChildren().add(textArea);
		
		MessageBox.createCustom("New Students", vbox).showAndWait();
		
		Arrays.stream(textArea.getText().split("\n")) // split into lines
				.map(name -> name.split(", ", 2)) // split into first and last names
				.filter(names -> names.length == 2) // remove any mis-formatted values
				.forEachOrdered(names -> {
					SeatingPlan.Student student = seatingPlan.newStudent();
					student.setLastName(names[0]); // set first name
					student.setFirstName(names[1]); // set last name
				});
	}
	
	public void deleteSelectedStudents(){
		seatingPlan.getStudents().removeAll(studentsTable.getSelectionModel().getSelectedItems()); // remove selected students
	}
	
	public void swapSelectedStudents(){
		List<SeatingPlan.Student> selected = studentsTable.getSelectionModel().getSelectedItems();
		
		if(selected.size() == 2){ // make sure only 2 students are selected
			// get both students
			SeatingPlan.Student a = selected.get(0);
			SeatingPlan.Student b = selected.get(1);
			
			// hold onto a's seat
			SeatingPlan.Seat tmp = a.getSeat();
			// set a's seat to b's seat
			a.setSeat(b.getSeat());
			// set b's seat to a's old seat (that we held onto)
			b.setSeat(tmp);
		}
	}
	
	public void assignStudentsAlphabetically(){
		permutateStudentsSeats(s -> s.sort(
				// sort by comparing "<Last Name><First Name>", ignoring case
				(a, b) -> (a.getLastName() + a.getFirstName()).compareToIgnoreCase(b.getLastName() + b.getLastName())
		));
	}
	
	public void assignStudentsRandomly(){
		permutateStudentsSeats(s -> {
			// pad list with null students to fill up all seats instead of first n seats
			while(s.size() < seatingPlan.getSeats().size()){
				s.add(null);
			}
			
			Collections.shuffle(s);
		});
	}
	
	/** Copies the list of students to another list and them applies the permutation and sets the seats based on each student's index */
	private void permutateStudentsSeats(Consumer<List<SeatingPlan.Student>> permutation){
		if(seatingPlan.getSeats().size() < seatingPlan.getStudents().size()){
			if(!MessageBox.showConfirm("Cannot Assign All Students", "There are not enough seats to assign all " +
					"students, some students will not be assigned. Are you sure you want to continue?")){
				return;
			}
		}
		
		// copy the list of students
		List<SeatingPlan.Student> permuted = new ArrayList<>(seatingPlan.getStudents());
		// apply the permutation
		permutation.accept(permuted);
		
		// assign students by their indices in the permuted list
		for(int i = 0; i < permuted.size() && i < seatingPlan.getSeats().size(); i++){
			seatingPlan.getSeats().get(i).setStudent(permuted.get(i));
		}
	}
	
	public void showHelp(){
		showWebDialog(Main.TITLE + " - Help", "/info/help.html");
	}
	
	public void showAbout(){
		showWebDialog(Main.TITLE + " - About", "/info/about.html");
	}
	
	/** Displays a web page from the project's resources */
	private void showWebDialog(String title, String resource){
		WebView webView = new WebView();
		
		webView.getEngine().load(getClass().getResource(resource).toExternalForm()); // load the web page
		webView.setContextMenuEnabled(false); // disable the right click menu
		
		Alert alert = MessageBox.createCustom(title, webView);
		alert.getDialogPane().setPrefWidth(900);
		alert.show();
	}
}
