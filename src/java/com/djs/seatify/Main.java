package com.djs.seatify;

import java.util.stream.IntStream;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application{
	public static final String TITLE = "If I Fits, I Sits";
	
	public static void main(String[] args){
		// Fix for combo box crashing on Windows 10 with touchscreen
		if(System.getProperty("os.name").contains("Windows 10")){
			System.setProperty("glass.accessible.force", "false");
		}
		
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/seatify.fxml"));
		Parent root = loader.load();
		
		primaryStage.setTitle(TITLE);
		primaryStage.setScene(new Scene(root));
		primaryStage.setMinWidth(854);
		primaryStage.setMinHeight(480);
		
		// instead of just closing the window, redirect the close request to SeatifyController.exit to show confirm exit dialog
		primaryStage.setOnCloseRequest(event -> {
			((SeatifyController) loader.getController()).exit();
			event.consume(); // consume the event so that the window isn't closed
		});
		
		// set window icons
		IntStream.of(16, 24, 32, 48, 64, 128, 256, 512, 1024) // get a stream of sizes
				.mapToObj(size -> "/icons/app_icon_" + size + ".png") // get the full file name from the size
				.map(Image::new) // create a new image from the file name
				.forEach(primaryStage.getIcons()::add); // add them to the window's icons list
		
		primaryStage.show();
	}
}
