package com.djs.seatify;

import com.djs.seatify.backend.SeatingPlan;
import com.sommd.fxutils.Bindings2;
import com.sommd.fxutils.Step;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 * Displays a preview of a {@link SeatingPlan}.
 */
public class SeatingPlanView extends Pane implements InvalidationListener{
	private final ObjectProperty<SeatingPlan> seatingPlan;
	
	private final IntegerBinding widthBinding, heightBinding;
	
	public SeatingPlanView(){
		this.seatingPlan = new SimpleObjectProperty<>(null);
		
		widthBinding = Bindings2.selectInteger(seatingPlan,
				(Step<SeatingPlan>) SeatingPlan::widthProperty);
		heightBinding = Bindings2.selectInteger(seatingPlan,
				(Step<SeatingPlan>) SeatingPlan::heightProperty);
	}
	
	public SeatingPlan getSeatingPlan(){
		return seatingPlan.get();
	}
	
	public ObjectProperty<SeatingPlan> seatingPlanProperty(){
		return seatingPlan;
	}
	
	public void setSeatingPlan(SeatingPlan seatingPlan){
		SeatingPlan old = this.seatingPlan.get();
		if(old != null){
			old.getSeats().removeListener(this);
		}
		
		this.seatingPlan.set(seatingPlan);
		
		if(seatingPlan != null){
			seatingPlan.getSeats().addListener(this);
			invalidated(null);
		}else{
			// clear seat nodes if set to null
			getChildren().clear();
		}
	}
	
	@Override
	public void invalidated(Observable observable){
		SeatingPlan plan = seatingPlan.get();
		
		if(plan != null){
			// resize children (seat nodes) to match seating plan
			ObservableList<SeatingPlan.Seat> seats = plan.getSeats();
			ObservableList<Node> nodes = getChildren();
			
			if(nodes.size() > seats.size()){
				// remove excess seat nodes
				nodes.remove(seats.size(), nodes.size());
			}else{
				while(nodes.size() < seats.size()){
					newSeatNode(nodes.size());
				}
			}
		}
	}
	
	/**
	 * Creates a node which displays the information of a seat
	 */
	private void newSeatNode(int i){
		VBox pane = new VBox();
		pane.setAlignment(Pos.TOP_CENTER);
		pane.setStyle("-fx-background-color: #8CE2FF; -fx-border-color: black;");
		
		// add seat number
		Label seatNumber = new Label();
		seatNumber.setText(String.format("Seat %d", i + 1));
		seatNumber.setTextAlignment(TextAlignment.CENTER);
		seatNumber.setUnderline(true);
		pane.getChildren().add(seatNumber);
		
		// binding to the seat for this seat node
		Binding<SeatingPlan.Seat> seatBinding = Bindings2.select(seatingPlan,
				(SeatingPlan x) -> Bindings.valueAt(x.getSeats(), i));
		
		// binding to the student for this seat node
		Binding<SeatingPlan.Student> studentBinding = Bindings2.select(seatBinding,
				(Step<SeatingPlan.Seat>) SeatingPlan.Seat::studentProperty);
		
		// add first name
		Label firstName = new Label();
		firstName.textProperty().bind(Bindings2.select(studentBinding,
				(Step<SeatingPlan.Student>) SeatingPlan.Student::firstNameProperty));
		pane.getChildren().add(firstName);
		
		// add last name
		Label lastName = new Label();
		lastName.textProperty().bind(Bindings2.select(studentBinding,
				(Step<SeatingPlan.Student>) SeatingPlan.Student::lastNameProperty));
		pane.getChildren().add(lastName);
		
		// create position bindings
		IntegerBinding xBinding = Bindings2.selectInteger(seatBinding,
				(Step<SeatingPlan.Seat>) SeatingPlan.Seat::positionXProperty);
		IntegerBinding yBinding = Bindings2.selectInteger(seatBinding,
				(Step<SeatingPlan.Seat>) SeatingPlan.Seat::positionYProperty);
		
		// bind layout position
		pane.layoutXProperty().bind(widthProperty().multiply(xBinding).divide(widthBinding));
		pane.layoutYProperty().bind(heightProperty().multiply(yBinding).divide(heightBinding));
		
		// bind size
		pane.setMinSize(0, 0);
		pane.prefWidthProperty().bind(widthProperty().divide(widthBinding));
		pane.prefHeightProperty().bind(heightProperty().divide(heightBinding));
		
		// add the pane
		getChildren().add(pane);
	}
}
